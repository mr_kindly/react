const products = [
    {
        id: 1,
        categoryId: 2,
        title: 'Product 1',
        description: 'Test product',
        price: {
            value: 1000,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    },
    {
        id: 2,
        categoryId: 2,
        title: 'Product 2',
        description: 'Test product 2',
        price: {
            value: 500,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    },
    {
        id: 3,
        categoryId: 2,
        title: 'Product 3',
        description: 'Test product 3',
        price: {
            value: 1000,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    },
    {
        id: 4,
        categoryId: 2,
        title: 'Product 4',
        description: 'Test product 4',
        price: {
            value: 500,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    },
    {
        id: 5,
        categoryId: 1,
        title: 'Product 5',
        description: 'Test product 5',
        price: {
            value: 1000,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    },
    {
        id: 6,
        categoryId: 1,
        title: 'Product 6',
        description: 'Test product 6',
        price: {
            value: 500,
            discount: 10,
            currency: "$"
        },
        imgUrl: 'http://irecommend.ru/sites/default/files/product-images/734681/zzvB4dBJtRsi2rKlLOQ.png'
    }
]



function getProduct(req, res) {
    const productId = req.params.id;
    const product = products.find(i => i.id.toString() === productId);
    res.status(200).json(product);
}

function getCategory(req, res) {
    console.info('here');
    const productId = req.params.id;
    res.status(200).json({
        id: 10,
        title: 'Category 1',
        description: 'Test category'
    });
}

function getCategories(req, res) {
    res.status(200).json([{
        id: 1,
        title: 'Category 1',
        description: 'Test category'
    },
    {
        id: 2,
        title: 'Category 2',
        description: 'Test category 2'
    }]);
}

function getCategoryProducts(req, res) {
    const categoryId = req.params.id;
    const filteredProducts = products.filter(p => p.categoryId.toString() === categoryId);
    res.status(200).json(filteredProducts)
}

module.exports = {
    getCategories,
    getCategory,
    getCategoryProducts,
    getProduct
};
