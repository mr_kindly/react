import actionType from './../../constants/actionTypes.js';
import { getCategoryProducts, getProduct } from './../../api.js';

export function productsFetchStart() {
    return { type: actionType.PRODUCTS_FETCH_START };
}

export function productsFetchSuccess(payload) {
    return { type: actionType.PRODUCTS_FETCH_SUCCESS, payload: payload };
}

export function productsFetchFail() {
    return { type: actionType.PRODUCTS_FETCH_FAIL };
}

export function fetchCategoryProducts(id) {

    return (dispatch) => {
        dispatch(productsFetchStart());
        return getCategoryProducts(id)
            .then((data) => dispatch(productsFetchSuccess(data)))
            .catch(() => dispatch(productsFetchFail()));
    }
}

export function productFetchStart() {
    return { type: actionType.PRODUCT_FETCH_START };
}

export function productFetchSuccess(payload) {
    return { type: actionType.PRODUCT_FETCH_SUCCESS, payload: payload };
}

export function productFetchFail() {
    return { type: actionType.PRODUCT_FETCH_FAIL };
}

export function fetchProduct(id) {

    return (dispatch) => {
        dispatch(productFetchStart());
        return getProduct(id)
            .then((data) => dispatch(productFetchSuccess(data)))
            .catch(() => dispatch(productFetchFail()));
    }
}
