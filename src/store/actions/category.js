import actionTypes from './../../constants/actionTypes.js';
import {getCategoryProducts, getCategories} from './../../api.js';

export function categoriesFetchStart() {
    return {type: actionTypes.CATEGORIES_FETCH_START};
}

export function categoriesFetchSuccess(payload) {
    return {type: actionTypes.CATEGORIES_FETCH_SUCCESS, payload: payload};
}

export function categoriesFetchFail() {
    return {type: actionTypes.CATEGORIES_FETCH_FAIL};
}

export function fetchCategories() {

    return(dispatch) => {
        dispatch(categoriesFetchStart());
        return getCategories()
        .then((data) => dispatch(categoriesFetchSuccess(data)))
        .catch(() => dispatch(categoriesFetchFail()));
    }
}

export function categoryFetchStart() {
    return {type: actionTypes.CATEGORY_FETCH_START};
}

export function categoryFetchSuccess(payload) {
    return {type: actionTypes.CATEGORY_FETCH_SUCCESS, payload: payload};
}

export function categoryFetchFail() {
    return {type: actionTypes.CATEGORY_FETCH_FAIL};
}

export function fetchCategory(id) {

    return(dispatch) => {
        dispatch(categoryFetchStart());
        return getCategory(id)
        .then((data) => dispatch(categoryFetchSuccess(data)))
        .catch(() => dispatch(categoryFetchFail()));
    }
}
