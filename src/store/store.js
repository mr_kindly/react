import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const middleWare = [thunk];

const { createLogger } = require('redux-logger');
const logger = createLogger({
    diff: true,
    collapsed: true
});

middleWare.push(logger);

const store = createStore(rootReducer, applyMiddleware(...middleWare));

export default store;