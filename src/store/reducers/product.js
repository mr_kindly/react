import actionTypes from './../../constants/actionTypes.js';

const defaultState = {
    data: null,
    loading: false,
    error: false
}

export default function (state = defaultState, { type, payload }) {
    switch (type) {

        case actionTypes.PRODUCT_FETCH_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.PRODUCT_FETCH_SUCCESS:
            return {
                ...state,
                data: payload,
                loading: false,
                error: false
            }
        case actionTypes.PRODUCT_FETCH_FAIL:
            return {
                ...state,
                loading: false,
                error: true
            }

        default:
            return state;
    }
}
