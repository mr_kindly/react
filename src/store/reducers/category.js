import actionTypes from './../../constants/actionTypes.js';

const defaultState = {
    categories: null,
    products: null,
    loading: false,
    error: false
}

export default function (state = defaultState, { type, payload }) {
    switch (type) {

        case actionTypes.CATEGORIES_FETCH_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CATEGORIES_FETCH_SUCCESS:
            return {
                ...state,
                categories: payload,
                loading: false,
                error: false
            }
        case actionTypes.CATEGORIES_FETCH_FAIL:
            return {
                ...state,
                loading: false,
                error: true
            }

        case actionTypes.PRODUCTS_FETCH_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.PRODUCTS_FETCH_SUCCESS:
            return {
                ...state,
                products: payload,
                loading: false,
                error: false
            }
        case actionTypes.PRODUCTS_FETCH_FAIL:
            return {
                ...state,
                loading: false,
                error: true
            }

        default:
            return state;
    }
}
