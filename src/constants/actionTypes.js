import keyMirror from 'keymirror';

export default keyMirror({
    CATEGORY_FETCH_START: true,
    CATEGORY_FETCH_SUCCESS: true,
    CATEGORY_FETCH_FAIL: true,
    CATEGORIES_FETCH_START: true,
    CATEGORIES_FETCH_SUCCESS: true,
    CATEGORIES_FETCH_FAIL: true,
    PRODUCT_FETCH_START: true,
    PRODUCT_FETCH_SUCCESS: true,
    PRODUCT_FETCH_FAIL: true,
    PRODUCTS_FETCH_START: true,
    PRODUCTS_FETCH_SUCCESS: true,
    PRODUCTS_FETCH_FAIL: true
});
